#!/usr/bin/env python3

"""Generates a combined CSV file using pandas."""
"""Generates a combined CSV file that is split into separate files
with a maximum number of rows per file."""


from datetime import date

import pymsgbox

from modules import csv_builder


def main() -> int:
    # Get filename from user
    merged_filename = pymsgbox.prompt("Enter a filename for the merged CSV file: ")
    if not merged_filename.lower().endswith(".csv"):
        merged_filename = "".join([merged_filename, ".csv"])

    # Save file
    try:
        csv_builder.joiner(merged_filename)
    except IOError:
        pymsgbox.alert("Error saving file, CSV files not merged!")
        return -1  # IOError, files not saved


    return 0


if __name__ == "__main__":
    main()
